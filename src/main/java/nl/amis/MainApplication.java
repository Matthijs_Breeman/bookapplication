package nl.amis;


import java.io.IOException;

public class MainApplication {
    public static void main(String[] args) throws IOException {
        BookFactory bookFactory = new BookFactory();
        Book book = bookFactory.createBook();
        System.out.println("Book created:");
        System.out.println(book);

    }
}
