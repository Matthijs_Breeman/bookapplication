package nl.amis;

public class ChapterTitle extends Title {
    private long chapterNumber;

    public ChapterTitle(String title, String font, long chapterNumber) {
        super(title, font);
        this.chapterNumber = chapterNumber;
    }

    public ChapterTitle(long chapterNumber) {
        super();
        this.chapterNumber = chapterNumber;
    }

    public ChapterTitle() {
    }

    @Override
    public String toString() {
        return "ChapterTitle{" +
                "chapterNumber=" + chapterNumber +
                ",Title=" + super.getTitle() +
                ",font=" + super.getFont() +
                '}';
    }

    public long getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(long chapterNumber) {
        this.chapterNumber = chapterNumber;
    }
}
