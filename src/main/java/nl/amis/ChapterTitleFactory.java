package nl.amis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ChapterTitleFactory {
    public ChapterTitle makeChapterTitle(long chapterNumber) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        ChapterTitle chapterTitle = new ChapterTitle(chapterNumber);
        System.out.print("We are making a new chapter!\nWhat will the Title of this chapter be? ");
        String title = input.readLine();
        chapterTitle.setTitle(title);
        System.out.print("What font would you like to use for this? ");
        String font = input.readLine();
        chapterTitle.setFont(font);
        System.out.println("Creating chapter: ");
        System.out.println(chapterTitle);
        return chapterTitle;

    }
}
