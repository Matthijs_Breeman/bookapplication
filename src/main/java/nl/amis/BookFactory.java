package nl.amis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BookFactory extends Object{
    private long bookId = 0;
    public Book createBook() throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        Book book = new Book();
        book.setId(bookId);
        bookId++;
        System.out.print("We are writing a book!\nWhat would you like to call it? ");
        String bookTitle = input.readLine();
        book.setTitle(bookTitle);
        System.out.print("The book shall be called " + book.getTitle() + " and it shall be my book.\n" +
                "What is you first name? ");
        String authorFirstName = input.readLine();
        book.setAuthorFirstName(authorFirstName);
        System.out.print("Hello " + authorFirstName +
                ". What is your last name? ");
        String authorLastName = input.readLine();
        book.setAuthorLastName(authorLastName);
        System.out.println("What is the ISBN of your book? (Hint: it always starts with 928 and is 13 characters long.) ");
        long isbn = 0;
        while (isbn == 0) {
            try {
                isbn = Long.parseLong(input.readLine());
            } catch (NumberFormatException e) {
                System.out.println("Unexpexted input, we want a number!");
            } catch (IOException e) {
                System.out.println("Unexpexted input, we want a number!");
            }
        }
        book.setIsbn(isbn);
        String makeNextChapter = "y";
        System.out.println("We will make a list of Chapters.");
        ChapterTitleFactory chapterTitleFactory = new ChapterTitleFactory();
        int chapterCounter = 0;
        String anotherChapter;
        while (makeNextChapter.equals("y")) {
            ChapterTitle chapterTitle = chapterTitleFactory.makeChapterTitle(chapterCounter);
            chapterCounter++;
            book.addChapterTitle(chapterTitle);
            System.out.println("Do you wish to create another chapter? (y/n) ");
            anotherChapter = input.readLine();
            System.out.println("interpreting: " + anotherChapter);
            if (anotherChapter.equals("y")){
                System.out.println("We shall create another chapter");
            }
            else if(anotherChapter.equals("n")){makeNextChapter = "n";}
            else {
                System.out.println("Unable to interpret meaning, making another chapter...");
            }

        }


        return book;
    }
}
