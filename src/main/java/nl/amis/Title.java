package nl.amis;

public class Title {
    private String title;
    private String font;

    public Title() {
    }

    public Title(String title, String font) {
        this.title = title;
        this.font = font;
    }

    @Override
    public String toString() {
        return "Title{" +
                "Title='" + title + '\'' +
                ", font='" + font + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }


}
