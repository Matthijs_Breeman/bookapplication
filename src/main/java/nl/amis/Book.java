package nl.amis;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private String title;
    private String authorFirstName;
    private String authorLastName;
    private long isbn;
    private long id;
    private List<ChapterTitle> listOfChapterTitles = new ArrayList<>();

    public Book() {
    }

    public Book(String title, String authorFirstName, String authorLastName, long isbn, long id) {
        this.title = title;
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.isbn = isbn;
        this.id = id;
    }

    @Override
    public String toString() {

        return "Book{" +
                "Title='" + title + '\'' +
                ", authorFirstName='" + authorFirstName + '\'' +
                ", authorLastName='" + authorLastName + '\'' +
                ", isbn=" + isbn +
                ", id=" + id +
                ", listOfChapterTitles=" + listOfChapterTitles +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<ChapterTitle> getListOfChapterTitles() {
        return listOfChapterTitles;
    }

    public void setListOfChapterTitles(List<ChapterTitle> listOfChapterTitles) {
        this.listOfChapterTitles = listOfChapterTitles;
    }

    public void addChapterTitle(ChapterTitle chapterTitle) {
        listOfChapterTitles.add(chapterTitle);
    }
}
